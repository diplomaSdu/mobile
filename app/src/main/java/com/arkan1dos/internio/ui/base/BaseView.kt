package com.arkan1dos.internio.ui.base

interface BaseView {
    fun onError(error: Error)
    fun showProgress()
    fun hideProgress()
}