package com.arkan1dos.internio.ui.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arkan1dos.internio.databinding.FavoriteFragmentBinding
import com.arkan1dos.internio.databinding.MainFragmentBinding
import com.arkan1dos.internio.ui.base.BasePresenterFragment
import org.koin.android.ext.android.inject

class FavoriteFragment : BasePresenterFragment<FavoritePresenter>(), FavoriteView {
    override val presenter: FavoritePresenter by inject()

    lateinit var binding: FavoriteFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FavoriteFragmentBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onError(error: Error) {

    }
}