package com.arkan1dos.internio.ui.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.arkan1dos.internio.R

class ThirdPage: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.onboarding_third_fragment, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var actionButton = view.findViewById<Button>(R.id.third_page_button_action)
        actionButton.setOnClickListener {
            onboarding.finishOnboarding()
        }

    }

    companion object {
        lateinit var onboarding: Onboarding
        fun newInstance(onboarding: Onboarding): ThirdPage {
            this.onboarding = onboarding
            return ThirdPage()
        }
    }
}