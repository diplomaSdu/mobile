package com.arkan1dos.internio.ui.onboarding

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.arkan1dos.internio.R
import com.github.appintro.AppIntro

class Onboarding : AppIntro() {
    private val slides = listOf(
        FirstPage.newInstance(this),
        SecondPage.newInstance(this),
        ThirdPage.newInstance(this)
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //FirstPage.newInstance(this)
        slides.forEach { addSlide(it) }

        isSkipButtonEnabled = false
        isIndicatorEnabled = false
        setDoneText("")
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            getDrawable(R.drawable.empty)?.let {
                setImageNextButton(it)
            }
        }
        isButtonsEnabled = false
    }

    public fun changePage() {
        goToNextSlide()
    }

    public fun finishOnboarding() {
        this.finish()
    }

    public override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
    }

    public override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        finishOnboarding()

    }

    public override fun onNextPressed(currentFragment: Fragment?) {
        super.onNextPressed(currentFragment)
        changePage()
    }
}