package com.arkan1dos.internio.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arkan1dos.internio.databinding.MainFragmentBinding
import com.arkan1dos.internio.ui.base.BasePresenterFragment
import org.koin.android.ext.android.inject

class MainFragment : BasePresenterFragment<MainPresenter>(), MainView {
    override val presenter: MainPresenter by inject()

    lateinit var binding: MainFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MainFragmentBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onError(error: Error) {

    }
}