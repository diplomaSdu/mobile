package com.arkan1dos.internio.ui.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.arkan1dos.internio.R

abstract class BaseFragment : Fragment(){


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun showProgress(enabled: Boolean) {

    }

    fun isLogged(): Boolean {
        return true
    }

    fun showAuth() {
    }

    inline fun <reified F : Fragment> getFragment(): F? {
        return requireFragmentManager().fragments.filter {
            it is F
        }.firstOrNull() as? F
    }

    open fun onResumeFragment() {}

    fun removeFragment(fragment: Fragment) {
        val transaction = requireFragmentManager().beginTransaction()
        transaction.remove(fragment)
        transaction.commitNow()
    }

    fun showFragment(
        fragment: Fragment?,
        nextFragment: Fragment,
        tag: String? = null,
        container: Int = R.id.base_container,
        backImmediate: Boolean = true,
        single: Boolean = true
    ) {
        val transaction = fragmentManager!!.beginTransaction()
        var newTag = tag
        if (!backImmediate) {
            newTag = "#$newTag"
        }
        if (single) {
            fragmentManager?.fragments?.firstOrNull {
                it.tag == newTag
            }?.let {
                removeFragment(it)
            }
        }
        if (fragment?.tag != "main") {
            /*transaction.setCustomAnimations(
                R.anim.jmart_slide_in_left,
                R.anim.jmart_slide_out_left,
                R.anim.jmart_slide_in_right,
                R.anim.jmart_slide_out_right
            )*/
            fragment?.let {
                transaction.hide(it)
            }
        }
        transaction.add(container, nextFragment, newTag)
        transaction.addToBackStack(newTag)
        transaction.commit()
    }


}