package com.arkan1dos.internio.ui.adapters
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter

class PagerFragmentAdapter(private val fm: FragmentManager, lifecycle: Lifecycle, private val fragments: List<Fragment>): FragmentStateAdapter(fm, lifecycle) {


    override fun getItemCount(): Int {
        return this.fragments.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }

    fun destroyAllFragments() {
        fragments.forEach {
            fm.beginTransaction().remove(it).commit()
        }
    }

}