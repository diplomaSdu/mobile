package com.arkan1dos.internio.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arkan1dos.internio.databinding.ProfileFragmentBinding
import com.arkan1dos.internio.ui.base.BasePresenterFragment
import org.koin.android.ext.android.inject

class ProfileFragment : BasePresenterFragment<ProfilePresenter>(), ProfileView {

    override val presenter: ProfilePresenter by inject()

    lateinit var binding: ProfileFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ProfileFragmentBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onError(error: Error) {

    }
}