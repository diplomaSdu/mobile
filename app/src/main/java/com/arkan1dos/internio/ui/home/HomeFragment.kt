package com.arkan1dos.internio.ui.home

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.view.iterator
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.arkan1dos.internio.R
import com.arkan1dos.internio.databinding.HomeFragmentBinding
import com.arkan1dos.internio.ui.adapters.PagerFragmentAdapter
import com.arkan1dos.internio.ui.base.BasePresenterFragment
import com.arkan1dos.internio.ui.event.EventFragment
import com.arkan1dos.internio.ui.event.EventFragmentContainer
import com.arkan1dos.internio.ui.favorite.FavoriteFragment
import com.arkan1dos.internio.ui.favorite.FavoriteFragmentContainer
import com.arkan1dos.internio.ui.main.MainFragment
import com.arkan1dos.internio.ui.main.MainFragmentContainer
import com.arkan1dos.internio.ui.profile.ProfileFragment
import com.arkan1dos.internio.ui.profile.ProfileFragmentContainer
import org.koin.android.ext.android.inject

class HomeFragment: BasePresenterFragment<HomePresenter>(),HomeView {
    override val presenter: HomePresenter by inject()

    val pages : List<Fragment> = listOf(
        MainFragment(),
        EventFragment(),
        FavoriteFragment(),
        ProfileFragment()
    )
    var adapter: PagerFragmentAdapter? = null
    var pager: ViewPager2? = null

    lateinit var binding: HomeFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            this.adapter =
                PagerFragmentAdapter(requireFragmentManager(), requireActivity().lifecycle, pages)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HomeFragmentBinding.inflate(inflater, container, false)
        this.pager = binding.contentPager
        pager?.offscreenPageLimit = 4
        pager?.adapter = adapter
        this.pager?.isUserInputEnabled = false
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //this.setTitle(getString(R.string.main))
        binding.bottomNavView.apply {
            setOnNavigationItemSelectedListener { menuItem ->
                when (menuItem.itemId) {

                    R.id.nav_main -> {
                    }

                    R.id.nav_event -> {
                    }

                    R.id.nav_favorite -> {
                    }

                    R.id.nav_profile -> {
                    }
                }
                menu.iterator().withIndex().forEach {
                    if (menuItem.itemId == it.value.itemId) {
                        pager?.setCurrentItem(it.index, true)
                    }
                }
                true
            }
        }
    }


    override fun onError(error: Error) {
    }
}