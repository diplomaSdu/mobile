package com.arkan1dos.internio.ui.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.arkan1dos.internio.R

class FavoriteFragmentContainer: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.favorite_fragment_container, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initFavoriteFragment()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    fun initFavoriteFragment() {
        val favoriteFragment = FavoriteFragment()
        favoriteFragment.arguments = arguments
        requireFragmentManager().beginTransaction()
            .replace(R.id.favorite_container, favoriteFragment, "favorite")
            .commit()
    }
}