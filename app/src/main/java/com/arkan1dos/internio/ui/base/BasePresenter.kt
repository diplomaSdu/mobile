package com.arkan1dos.internio.ui.base

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import org.koin.core.KoinComponent
import java.lang.ref.WeakReference

abstract class BasePresenter <V : BaseView> : KoinComponent,LifecycleObserver {

    private var _baseView: WeakReference<V?> = WeakReference(null)
    val baseView: V?
        get() {
            return _baseView.get()
        }

    protected abstract fun observe(lifecycleOwner: LifecycleOwner)

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResumeEvent(lifecycle: LifecycleOwner) {
        (lifecycle as? V)?.let {
            this._baseView = WeakReference(it)
        }
        this.observe(lifecycle)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPauseEvent() {
        this._baseView = WeakReference(null)
    }

}