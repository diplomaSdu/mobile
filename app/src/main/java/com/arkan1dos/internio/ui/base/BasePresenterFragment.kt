package com.arkan1dos.internio.ui.base

import android.os.Bundle

abstract class BasePresenterFragment <P: BasePresenter<*>>: BaseFragment(),BaseView {
    protected abstract val presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(presenter)
        setHasOptionsMenu(true)
    }

    override fun showProgress() {
        showProgress(true)
    }

    override fun hideProgress() {
        showProgress(false)
    }
}