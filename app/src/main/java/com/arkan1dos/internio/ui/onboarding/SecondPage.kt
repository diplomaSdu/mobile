package com.arkan1dos.internio.ui.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.arkan1dos.internio.R

class SecondPage: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.onboarding_second_fragment, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var actionButton = view.findViewById<LinearLayout>(R.id.second_page_acion)
        actionButton.setOnClickListener {
            onboarding.changePage()
        }
    }

    companion object {
        lateinit var onboarding: Onboarding
        fun newInstance(onboarding: Onboarding): SecondPage {
            this.onboarding = onboarding
            return SecondPage()
        }
    }
}