package com.arkan1dos.internio.di

import com.arkan1dos.internio.ui.event.EventPresenter
import com.arkan1dos.internio.ui.favorite.FavoritePresenter
import com.arkan1dos.internio.ui.home.HomePresenter
import com.arkan1dos.internio.ui.main.MainPresenter
import com.arkan1dos.internio.ui.profile.ProfilePresenter
import org.koin.dsl.module

object KoinModules {
    val presentersModule = module {
        single { HomePresenter() }
        single { MainPresenter() }
        single { EventPresenter() }
        single { FavoritePresenter() }
        single { ProfilePresenter() }
    }
}