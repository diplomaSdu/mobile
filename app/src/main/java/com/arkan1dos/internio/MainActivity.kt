package com.arkan1dos.internio

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.arkan1dos.internio.di.KoinModules
import com.arkan1dos.internio.ui.base.BaseFragment
import com.arkan1dos.internio.ui.home.HomeFragment
import com.arkan1dos.internio.ui.home.InterioFragment
import com.arkan1dos.internio.ui.onboarding.Onboarding
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MainActivity : AppCompatActivity() {
    private val settings by inject<InternioSettings>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startKoin {
            androidContext(applicationContext)
            modules(mutableListOf(
                KoinModules.presentersModule
            ))
        }

        supportFragmentManager.beginTransaction().replace(R.id.main_container, InterioFragment())
            .commit()
        supportFragmentManager.addOnBackStackChangedListener {
            supportFragmentManager.fragments.firstOrNull {
                it is HomeFragment
            }?.let {
                if (it.isVisible) {
                    (it as? BaseFragment)?.onResumeFragment()
                }
            }
        }
        //if (settings.getFirstEnter()) {
            val intent = Intent(this, Onboarding::class.java)
            startActivity(intent)
        //}
    }

    override fun onResume() {
        super.onResume()

    }
}