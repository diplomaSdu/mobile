package com.arkan1dos.internio

import android.content.Context
import android.provider.Settings
import com.securepreferences.SecurePreferences

class InternioSettings (context: Context) {
    companion object {
        private const val SECURE_PREF = "settings"
        private const val FIRST_ENTER = "first_enter"
    }
    private val settings = SecurePreferences(
        context,
        Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID),
        SECURE_PREF
    )

    fun saveFirstEnter(){
        this.settings.edit().putBoolean(FIRST_ENTER,false).commit()
    }

    fun getFirstEnter(): Boolean{
        return this.settings.getBoolean(FIRST_ENTER,true)
    }
}